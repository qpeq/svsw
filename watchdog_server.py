import subprocess
import threading
from time import time
from base_server import BaseServer
from win32_named_pipe import Win32Pipe
from logger import logger

GUID = '8124124c-9ae2-43cf-9fad-3e5ec4f010ee'
PROGRAM_NAME = 'VPWatchDog'
MUTEX_NAME = PROGRAM_NAME + GUID

class WatchDogServer(BaseServer):
    FORCE_KILL_TIME = 10
    def __init__(self):
        super(WatchDogServer, self).__init__(self._run, self._end, 1)
        self._proc = None
        self._commanline = 'notepad'
        self._shutdown_pipe = False
        self._pipe = None
        self._pipe_thread = threading.Thread(target=self._read_pipe)
        self._pipe_thread.start()

    def _read_pipe(self):
        while not self._shutdown_pipe:
            self._pipe = Win32Pipe(MUTEX_NAME)
            action = self._pipe.read()
            if action == 'start':
                self._do_start()
            elif action == 'stop':
                self._do_stop()
            self._pipe.close()

    def _run(self):
        try:
            if self._proc and self._proc.poll() is not None:
                logger.error('The child is not running, trying to restart...')
                self._proc = None
                self._do_start()
        except Exception, e:
            logger.error(e)

    def _end(self):
        start_time = time()
        while self._proc and self._proc.poll() is None:
            curr_time = time()
            if curr_time - start_time < self.FORCE_KILL_TIME:
                logger.info('send SIGTERM to the child process.')
            else:
                self._proc.terminate()
                logger.info('Killing the child process.')
                self._proc.kill()

        self._proc = None
        self._pipe.close()
        self._shutdown_pipe = True
        self._pipe_thread.join()

    def _run_proc(self):
        if self._proc is None:
            self._proc = subprocess.Popen(self._commanline, shell=False, stdout=subprocess.PIPE)

    def _do_start(self):
        self._run_proc()

    def _do_stop(self):
        self.shutdown()
