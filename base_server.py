from time import sleep
from threading import Event

class BaseServer(object):
    def __init__(self, run_fun, end_fun, sleep_time=0):
        self.__is_shut_down = Event()
        self.__shutdown_request = False
        self._sleep_time = sleep_time
        self._run_fun = run_fun
        self._end_fun = end_fun

    def serve_forever(self):
        self.__is_shut_down.clear()
        try:
            while not self.__shutdown_request:
                if self._run_fun:
                    self._run_fun()
                if self._sleep_time:
                    sleep(self._sleep_time)
        finally:
            self.__shutdown_request = False
            self.__is_shut_down.set()
            if self._end_fun:
                self._end_fun()

    def shutdown(self):
        self.__shutdown_request = True
        self.__is_shut_down.wait()
