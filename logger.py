import logging
from logging.handlers import RotatingFileHandler
LOG_FILE_SIZE = 1024 * 1024

# logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)-8s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')
handler = RotatingFileHandler(__name__ + '.log', 'a', LOG_FILE_SIZE, 10)
formatter = logging.Formatter('%(asctime)-12s: %(levelname)-8s %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
handler.setFormatter(formatter)
logger = logging.getLogger(__name__)
logger.addHandler(handler)


if __name__ == '__main__':
    logger.info('fuck')
