import sys
import logging
from logging.handlers import RotatingFileHandler
import argparse
from named_mutex import NamedMutex
from win32_named_pipe import Win32Pipe
from logger import logger
from watchdog_server import MUTEX_NAME
from watchdog_server import WatchDogServer

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--daemon', help='Run as a daemon', action='store_true')
    parser.add_argument('-s', '--start', help='Start the service', action='store_true')
    parser.add_argument('-t', '--stop', help='Stop the service', action='store_true')
    args = parser.parse_args()
    if args.daemon:
        try:
            mutex = NamedMutex(MUTEX_NAME)
        except AttributeError:
            logger.error('The instance already exists')
            exit(0)
        server = WatchDogServer()
        server.serve_forever()
    elif args.start:
        try:
            pipe = Win32Pipe(MUTEX_NAME, True)
            pipe.write('start')
        except:
            logger.error('The daemon is not running')
    elif args.stop:
        try:
            pipe = Win32Pipe(MUTEX_NAME, True)
            pipe.write('stop')
        except:
            logger.error('The daemon is not running')
    else:
        parser.print_help()
