import win32service  
import win32serviceutil  
import win32event  

SERVICE_NAME = 'VVTKWatchDog'
SERVICE_DISPLAY_NAME = 'Vivotek Watch Dog'
SERVICE_DESCRIPTION = 'A watch dog for Vivotek remote server'


class PyWindowsService(win32serviceutil.ServiceFramework):
    # you can NET START/STOP the service by the following name
    _svc_name_ = SERVICE_NAME
    # this text shows up as the service name in the Service
    # Control Manager (SCM)
    _svc_display_name_ = SERVICE_DISPLAY_NAME
    # this text shows up as the description in the SCM
    _svc_description_ = SERVICE_DESCRIPTION

    def __init__(self, args):
        win32serviceutil.ServiceFramework.__init__(self, args)
        # create an event to listen for stop requests on
        self.hWaitStop = win32event.CreateEvent(None, 0, 0, None)

    # core logic of the service
    def SvcDoRun(self):
        # import servicemanager

        rc = None
        # if the stop event hasn't been fired keep looping
        while rc != win32event.WAIT_OBJECT_0:
            # block for 5 seconds and listen for a stop event
            rc = win32event.WaitForSingleObject(self.hWaitStop, 5000)

    # called when we're being shut down
    def SvcStop(self):
        # tell the SCM we're shutting down
        self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
        # fire the stop event
        win32event.SetEvent(self.hWaitStop)

if __name__ == '__main__':
    def run():
        print 'ok'

    win32serviceutil.HandleCommandLine(PyWindowsService)
