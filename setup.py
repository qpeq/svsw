from distutils.core import setup
import py2exe

setup(
    windows = [{
        "script": "main.py",
        # "icon_resources": [(0, "favicon.ico")], 
        "dest_base" : "mywatchdog"
    }],
    zipfile = None,
    options = {'py2exe': {'bundle_files': 1, 'compressed': True, "includes":[]}}
    )